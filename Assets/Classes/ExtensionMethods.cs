﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    /// <summary>
    /// Extension version of Mathf.Abs()
    /// </summary>
    public static float Abs(this float _float)
    {
        return Mathf.Abs(_float);
    }
}
