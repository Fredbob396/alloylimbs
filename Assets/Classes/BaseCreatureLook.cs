﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class BaseCreatureLook : MonoBehaviour
{
    [SerializeField] internal GameObject Looker;
    [SerializeField] internal GameObject Aimer;
    [SerializeField] internal Transform HorizontalLook;
    [SerializeField] internal Transform VerticalLook;

    private float _xAxisClamp;

    /// <summary>
    /// Rotate the look transforms by the specified degrees on both axes
    /// </summary>
    internal void LookByDegrees(float rotX, float rotY)
    {
        // Track clamp separately from transform due to euler angle wierdness
        _xAxisClamp += rotY;

        // Prevent look overflow up
        if (_xAxisClamp > 90.0f)
        {
            _xAxisClamp = 90.0f;
            rotY = 0.0f;
        }

        // Prevent look overflow down
        else if (_xAxisClamp < -90.0f)
        {
            _xAxisClamp = -90.0f;
            rotY = 0.0f;
        }

        // Apply rotation
        HorizontalLook.Rotate(Vector3.up * rotX, Space.World);
        VerticalLook.Rotate(Vector3.left * rotY, Space.Self);
    }

    internal void HandleAim(int layerMask)
    {
        Vector3Helpers.AimObjectAtFocusOfOther(Aimer, Looker, Looker.transform.forward, layerMask);
    }
}
