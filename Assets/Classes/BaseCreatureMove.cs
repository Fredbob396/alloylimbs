﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class BaseCreatureMove : MonoBehaviour
{
    // Look direction transforms
    [SerializeField] internal Transform HorizontalLook;
    [SerializeField] internal Transform VerticalLook;

    // Creature Components
    [SerializeField] internal Rigidbody Body;
    [SerializeField] internal BoxCollider Collider;

    // Public creature movement fields
    [SerializeField] internal float MoveSpeedLimit = 8.8f;
    [SerializeField] internal float MoveForce = 35.0f;
    [SerializeField] internal int AirMoveForceDivisor = 6;
    [SerializeField] internal float GroundDrag = 0.05f;
    [SerializeField] internal float AirDrag = 0.005f;

    // Other physics fields
    [SerializeField] internal float ColliderFriction = 0.4f;
    [SerializeField] internal int MaxWalkableIncline = 46;

    // Collision tracking variables
    internal bool IsGrounded { get; private set; }
    internal bool IsSliding { get; private set; }

    private RaycastHit _groundHit;
    private float _slopeTrueAngle;
    private Vector3 _slopeRelativeDirection;
    private Vector3 _slopePerpendicularDirection;

    // Movement tracking variables
    private Vector3 _queuedForce;

    // Jump tracking variables
    internal bool JumpQueued;

    private float _jumpForce;
    private bool _jumpOverridesVerticalMomentum;

    /// <summary>
    /// Overrides default unity physics drag in order to restrict it to X and Z axes only
    /// </summary>
    internal void LocalDrag()
    {
        var velocity = Body.velocity;
        float absX = Math.Abs(velocity.x);
        float absZ = Math.Abs(velocity.z);

        float dragFactor = IsGrounded
            ? 1.0f - GroundDrag
            : 1.0f - AirDrag;

        // Apply drag as long as X velocity is > 0
        if (absX > 0)
        {
            velocity.x = velocity.x * dragFactor;
            absX = Math.Abs(velocity.x);
            if (absX < 0 || absX < 0.001)
            {
                velocity.x = 0;
            }
        }

        // Apply drag as long as Z > 0
        if (absZ > 0)
        {
            velocity.z = velocity.z * dragFactor;
            absZ = Math.Abs(velocity.z);
            if (absZ < 0 || absZ < 0.001)
            {
                velocity.z = 0;
            }
        }

        Body.velocity = velocity;
    }

    /// <summary>
    /// Retrieve information about the creatures's state in relation to the geometry beneath them
    /// </summary>
    internal void GroundCheck()
    {
        // Sphere cast origin starts at origin of creature object
        Vector3 origin = transform.position;
        Quaternion rotation = transform.rotation;

        // Box-cast size matches collision XZ, but a Y that is 1/4 the height of the collision
        Vector3 boxCast = new Vector3(Collider.size.x, Collider.size.y / 4, Collider.size.z);
        
        // Distance is equal to 1/4 the height + 0.1f. This reaches just beneath the creature's collision
        float castDistance = Collider.size.y / 4 + 0.1f;

        IsGrounded = Physics.BoxCast(origin, boxCast, Vector3.down, out _groundHit, rotation, castDistance);

        // Calculate slope angles and handle sliding when grounded
        if (IsGrounded)
        {
            // Ground hit "normal" is perpendicular to the slope
            _slopePerpendicularDirection = _groundHit.normal;

            // Calculate slope angle
            _slopeTrueAngle = Vector3.Angle(_groundHit.normal, Vector3.down) - 90;

            // Get relative direction of the slope
            var tempRightAxis = Vector3.Cross(VerticalLook.forward.normalized, -_groundHit.normal);
            _slopeRelativeDirection = Vector3.Cross(tempRightAxis, _groundHit.normal);

            //Debug.DrawRay(VerticalAnchorTransform.position, _slopeRelativeDirection, Color.red);
            //Debug.DrawRay(VerticalAnchorTransform.position, _slopePerpendicularDirection, Color.blue);

            bool tooSteep = _slopeTrueAngle < MaxWalkableIncline;
            if (tooSteep && !IsSliding)
            {
                Slide();
            }
            else if (!tooSteep && IsSliding)
            {
                RegainTraction();
            }
        }
    }

    /// <summary>
    /// Set sliding and remove friction from collider
    /// </summary>
    private void Slide()
    {
        Collider.material.staticFriction = 0.0f;
        Collider.material.dynamicFriction = 0.0f;
        IsSliding = true;
        Collider.material = Collider.material;
    }

    /// <summary>
    /// Set not sliding and re-add friction to collider
    /// </summary>
    private void RegainTraction()
    {
        Collider.material.staticFriction = ColliderFriction;
        Collider.material.dynamicFriction = ColliderFriction;
        IsSliding = false;
        Collider.material = Collider.material;
    }

    /// <summary>
    /// Get an adjusted vector accounting for the relative (to the creature) slope given a direction
    /// </summary>
    private Vector3 GetRelativeSlopeDirection(Vector3 moveDirection)
    {
        var tempRightAxis = Vector3.Cross(moveDirection, -_groundHit.normal);
        var relativeSlopeDirection = Vector3.Cross(tempRightAxis, _groundHit.normal);

        return relativeSlopeDirection;
    }

    /// <summary>
    /// Queues movement force in a given direction
    /// </summary>
    internal void QueueMovementForce(Vector3 direction)
    {
        // Speed limiter
        float velocityInMovementDirection = Vector3.Dot(Body.velocity, direction);
        if (velocityInMovementDirection > MoveSpeedLimit)
        {
            _queuedForce = Vector3.zero;
            return;
        }

        Vector3 finalMovement;
        if (IsGrounded && !IsSliding)
        {
            finalMovement = GetRelativeSlopeDirection(direction) * MoveForce * Time.fixedDeltaTime;
        }
        else
        {
            finalMovement = direction * (MoveForce / AirMoveForceDivisor) * Time.fixedDeltaTime;
        }

        // Queue force for FixedUpdate loop
        _queuedForce = finalMovement;
    }

    /// <summary>
    /// Queues a jump with a given force and, optionally, whether this force overrides the current momentum
    /// </summary>
    internal void QueueJumpForce(float jumpForce, bool jumpOverridesVerticalMomentum = false)
    {
        JumpQueued = true;
        _jumpForce = jumpForce;
        _jumpOverridesVerticalMomentum = jumpOverridesVerticalMomentum;
    }

    /// <summary>
    /// Removes the queued jump force
    /// </summary>
    internal void DeQueueJumpForce()
    {
        _jumpForce = 0;
        JumpQueued = false;
        _jumpOverridesVerticalMomentum = false;
    }

    internal void ApplyQueuedForces()
    {
        // Apply queued jump force
        if (JumpQueued)
        {
            // For initial and double jumps, reset all vertical velocity to
            // prevent inconsistent jump height. Otherwise, apply force as normal.
            if (_jumpOverridesVerticalMomentum)
            {
                Vector3 newVelocity = Body.velocity;

                // Apply directional jump force if on a "steep" slope
                if (IsGrounded && Math.Abs(_slopeTrueAngle - 90) > 5)
                {
                    Body.AddForce(_slopePerpendicularDirection * _jumpForce, ForceMode.VelocityChange);
                }
                // Else, totally override y-axis (Prevents certain hop-timings not working on flat surfaces.)
                else
                {
                    newVelocity.y = _jumpForce;
                }

                Body.velocity = newVelocity;
            }
            else
            {
                // Apply queued jump force
                Body.AddForce(new Vector3(0, _jumpForce, 0), ForceMode.VelocityChange);
            }

            // ALWAYS un-queue
            JumpQueued = false;
        }

        // Apply queued movment force
        Body.AddForce(_queuedForce, ForceMode.Impulse);
    }
}