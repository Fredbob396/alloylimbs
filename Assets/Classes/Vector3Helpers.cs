﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vector3Helpers
{
    /// <summary>
    /// Rotates a Vector3 via Vector3.RotateTowards and then returns its equivalent Quarternion.LookRotation
    /// </summary>
    public static Quaternion LookTowardsRotatedVector(Vector3 current, Vector3 target, float maxRadiansDelta, float maxMagnitudeDelta)
    {
        Vector3 rotationVector = Vector3.RotateTowards(current, target, maxRadiansDelta, maxMagnitudeDelta);
        return Quaternion.LookRotation(rotationVector);
    }

    /// <summary>
    /// Removes Y-Axis from the vector and returns the normalized direction
    /// </summary>
    public static Vector3 GetXZDirection(Vector3 vec3)
    {
        vec3.y = 0.0f;
        return vec3.normalized;
    }

    /// <summary>
    /// Aims an object at the point which another object is looking.
    /// </summary>
    /// <param name="aimer">The object to aim</param>
    /// <param name="looker">The object that is looking</param>
    /// <param name="lookDirection">The direction the looker object is looking</param>
    /// <param name="maxDistance">The maximum distance to check for focus</param>
    public static void AimObjectAtFocusOfOther(GameObject aimer, GameObject looker, Vector3 lookDirection, int layerMask, float maxDistance = 10000.0f)
    {
        // Draw a ray from the looker in the specified direction
        RaycastHit lookerHit;
        bool lookHasHit = Physics.Raycast(new Ray(looker.transform.position, lookDirection), out lookerHit, maxDistance, layerMask);

        // If the looker raycast hits something, aim towards the hit point with the aimer
        if (lookHasHit)
        {
            aimer.transform.LookAt(lookerHit.point);

            // Debug: Draw looker hit point vector
            //Debug.DrawRay(looker.transform.position, looker.transform.forward * maxDistance, Color.cyan);
        }
        // Else, aim towards an imaginary point
        else
        {
            Vector3 distantPoint = looker.transform.position + (looker.transform.forward * maxDistance);
            aimer.transform.LookAt(distantPoint);

            // Debug: Draw distant point vector
            //Debug.DrawLine(looker.transform.position, looker.transform.position + (looker.transform.forward * maxDistance), Color.yellow);
        }

        // Debug: Draw aim vector
        Debug.DrawLine(aimer.transform.position, aimer.transform.position + (aimer.transform.forward * maxDistance), Color.magenta);
    }
}