﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : BaseCreatureMove
{
    // Public input fields
    [SerializeField] private string _horizontalInputName = "Horizontal";
    [SerializeField] private string _verticalInputName = "Vertical";
    [SerializeField] private string _jumpInputName = "Jump";

    // Public player jump fields
    [SerializeField] internal float JumpStage1Force = 6.0f;
    [SerializeField] internal float JumpStage2Force = 1.0f;
    [SerializeField] internal float JumpStage3Force = 1.5f;
    [SerializeField] internal float DoubleJumpForce = 5.0f;
    [SerializeField] internal float DoubleJumpNegativeVelocityLimit = 5.0f;
    [SerializeField] internal float JumpStage2Delay = 0.06f;
    [SerializeField] internal float JumpStage3Delay = 0.12f;

    // Private player jump fields
    private int _jumpStage;
    private float _jumpTimer;

    /// <summary>
    /// IMPORTANT: Prevents erroneous ground detection during jumps.
    /// allows time for the player to leave the ground before checking
    /// if the player is grounded.
    /// </summary>
    private float _ignoreGroundDelay = 0.05f;

    private void Update()
    {
        // Player input here
        CalculatePlayerMove();
        CalculatePlayerJump();
    }

    private void FixedUpdate()
    {
        // Physics updates here
        base.LocalDrag();
        base.GroundCheck();
        base.ApplyQueuedForces();
    }

    /// <summary>
    /// Calculates player movment
    /// </summary>
    private void CalculatePlayerMove()
    {
        float horizontalInput = Input.GetAxis(_horizontalInputName);
        float verticalInput = Input.GetAxis(_verticalInputName);

        //Debug.Log($"{horizontalInput},{verticalInput}");

        // Get vector in direction of movement reletive to axes
        Vector3 leftRightMovement = HorizontalLook.right * horizontalInput;
        Vector3 forwardMovement = HorizontalLook.forward * verticalInput;

        // Clamp vector to 1.0f to prevent faster diagonal movement

        Vector3 combinedDirection = Vector3.ClampMagnitude(leftRightMovement + forwardMovement, 1.0f);

        base.QueueMovementForce(combinedDirection);
    }

    /// <summary>
    /// Calculates player jumping
    /// </summary>
    /// STAGES:
    /// 0: The player is ready to begin another jump
    /// 1: Initial hop executed
    /// 2: Mid-jump executed
    /// 3: Max-jump executed
    /// 4: Ready for double jump
    /// 5: Double-jump executed
    private void CalculatePlayerJump()
    {
        // Don't bother with any calculations if awaiting the physics loop to execute a jump stage
        if (JumpQueued) return;

        // Track the airtime of the jump for certain timings
        if (_jumpStage > 0)
        {
            _jumpTimer += Time.deltaTime;
        }

        if (Input.GetAxisRaw(_jumpInputName) > 0.0f)
        {
            // The initial "kick" for the jump. (Hopping)
            if (_jumpStage == 0 && IsGrounded)
            {
                base.QueueJumpForce(JumpStage1Force, true);
                _jumpStage = 1;
            }
            // Middle-height jump
            else if (_jumpStage == 1 && _jumpTimer >= JumpStage2Delay)
            {
                base.QueueJumpForce(JumpStage2Force);
                _jumpStage = 2;
            }
            // Full jump
            else if (_jumpStage == 2 && _jumpTimer >= JumpStage3Delay)
            {
                base.QueueJumpForce(JumpStage3Force);
                _jumpStage = 3;
            }
            // Double jump
            else if (_jumpStage == 4 && !IsSliding)
            {
                // TODO: Maybe find a better place for this
                // Don't double jump if falling velocity is above a certain limit
                float fallVelocity = Vector3.Dot(Body.velocity, Vector3.down);
                if (!(fallVelocity > DoubleJumpNegativeVelocityLimit))
                {
                    base.QueueJumpForce(DoubleJumpForce, true);
                    _jumpStage = 5;
                }
            }
            // Remove force betwwen jump stages
            else if (_jumpStage == 1 && !(_jumpTimer >= JumpStage2Delay) ||
                     _jumpStage == 2 && !(_jumpTimer >= JumpStage3Delay))
            {
                base.DeQueueJumpForce();
            }
        }
        // If the player lets go of jump at any point, the rest of the jump is cancelled
        else if (!IsGrounded && _jumpTimer > _ignoreGroundDelay)
        {
            // Set to stage 4 if double jump not already executed (Stage 5)
            _jumpStage = Math.Max(_jumpStage, 4);
            _jumpTimer = 0;
            base.DeQueueJumpForce();
        }
        // Reset the jump when 100% grounded
        else if (_jumpStage != 0 && _jumpTimer > _ignoreGroundDelay)
        {
            _jumpStage = 0;
            _jumpTimer = 0;
            base.DeQueueJumpForce();
        }
    }
}
