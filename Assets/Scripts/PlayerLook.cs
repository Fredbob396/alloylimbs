﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : BaseCreatureLook
{
    [SerializeField] private GameObject _cameraRoot;
    [SerializeField] private GameObject _imaginaryCamera;
    [SerializeField] private GameObject _imaginaryCameraRoot;

    [SerializeField] private string _mouseXInputName = "Mouse X";
    [SerializeField] private string _mouseYInputName = "Mouse Y";
    [SerializeField] private float _mouseSensitivity = 150.0f;

    private float _cameraWallClearence = -0.25f;

    private void Awake()
    {
        // Lock cursor to window
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        base.HandleAim(~(1 << 9));
    }

    private void FixedUpdate()
    {
        Look();
        HandleCameraCollision();
    }

    private void Look()
    {
        // Get both axes using either the mouse or the joystick
        float inputX = Input.GetAxis(_mouseXInputName);
        float inputY = Input.GetAxis(_mouseYInputName);

        if (inputX == 0.0f)
            inputX = Input.GetAxis("RHorizontal");

        if (inputY == 0.0f)
            inputY = Input.GetAxis("RVertical");

        inputX *= _mouseSensitivity;
        inputX *= Time.deltaTime;

        inputY *= _mouseSensitivity;
        inputY *= Time.deltaTime;

        //Debug.Log($"{inputX},{inputY}");

        LookByDegrees(inputX, inputY);
    }

    // TODO: Minor camera clipping issues
    // TODO: Find an elegant way to only cast when necessary
    private void HandleCameraCollision()
    {
        // Step 1: Raycast from center of top of head to the CameraRoot (cr)
        Vector3 headTop = VerticalLook.position;
        headTop.y += 1;

        var crDist = Vector3.Distance(headTop, _imaginaryCameraRoot.transform.position);
        Vector3 crCheckDir = (_imaginaryCameraRoot.transform.position - headTop).normalized;

        bool crCheckHit = Physics.Raycast(headTop, crCheckDir, out RaycastHit crHit, crDist, ~(1 << 9));

        //Debug.DrawLine(headTop, _imaginaryCameraRoot.transform.position, Color.blue);
        if (crCheckHit)
        {
            // If hit, move camera root to point at which ray hit object
            // (Adjusted slightly back to avoid bad raycast)
            _cameraRoot.transform.position = crHit.point;
            _cameraRoot.transform.Translate(crCheckDir * _cameraWallClearence, Space.World);
        }
        else
        {
            // Else, reset back to its local position
            _cameraRoot.transform.localPosition = _imaginaryCameraRoot.transform.localPosition;
        }

        // Step 2: Raycast from CameraRoot to Camera (c)
        var cDist = Vector3.Distance(_imaginaryCameraRoot.transform.position, _imaginaryCamera.transform.position);
        Vector3 cCheckDir = Looker.transform.forward * -1;

        bool cCheckHit = Physics.Raycast(_cameraRoot.transform.position, cCheckDir, out RaycastHit cHit, cDist, ~(1 << 9));

        //Debug.DrawLine(_cameraRoot.transform.position, _imaginaryCamera.transform.position, Color.red);
        if (cCheckHit)
        {
            // If hit, move camera to point at which ray hit object
            // (Adjusted slightly back to avoid clipping [May be insufficient!])
            Looker.transform.position = cHit.point;
            Looker.transform.Translate(cCheckDir * _cameraWallClearence, Space.World);
        }
        else
        {
            // Else, reset back to its local position
            Looker.transform.localPosition = _imaginaryCamera.transform.localPosition;
        }

    }

}
