﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyRotate : MonoBehaviour
{
    [SerializeField] private string _horizontalInputName = "Horizontal";
    [SerializeField] private string _verticalInputName = "Vertical";
    [SerializeField] private GameObject _head;
    [SerializeField] private GameObject _torso;
    [SerializeField] private GameObject _legs;
    [SerializeField] private GameObject _horizontalLook;
    [SerializeField] private GameObject _verticalLook;
    [SerializeField] private Camera _camera;

    [SerializeField] private BaseCreatureMove _moveScript;

    private Rigidbody _body;

    // Start is called before the first frame update
    void Start()
    {
        _body = transform.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        HandleHeadRotation();
        HandleTorsoRotation();
        HandleLegsRotation();
    }

    private void HandleHeadRotation()
    {
        // TODO: Modify vertical angle by percentage of vertical?
        // Set the head's rotation to the horizontal anchor's
        _head.transform.rotation = _horizontalLook.transform.rotation;
    }

    private void HandleTorsoRotation()
    {
        // TODO: Modify vertical angle by percentage of vertical?
        // Set the torso's rotation to the horizontal anchor's
        _torso.transform.rotation = _horizontalLook.transform.rotation;
    }

    private void HandleLegsRotation()
    {
        // TODO: Separate input similar to how PlayerMove was handled
        var horizontalInput = Input.GetAxisRaw(_horizontalInputName);
        var verticalInput = Input.GetAxisRaw(_verticalInputName);

        Vector3 legRotation;

        // If strafing (Any horizontal input and vertical input not above a certain threshold), snap to 90 degrees
        if (horizontalInput != 0.0f && verticalInput.Abs() <= 0.10f && _moveScript.IsGrounded && !_moveScript.IsSliding)
        {
            // Dumb hack: Bump legs towards torso center when >= 90 degrees
            float torsoLegsDirectionDiff = Vector3.Angle(_torso.transform.forward, _legs.transform.forward);
            if (torsoLegsDirectionDiff >= 90.0f)
            {
                _legs.transform.rotation = Vector3Helpers.LookTowardsRotatedVector(_legs.transform.forward, _torso.transform.forward, 5 * Time.deltaTime, 0.0f);
            }

            // Queue rotation in direction of strafe
            legRotation = horizontalInput > 0
                ? _torso.transform.right
                : _torso.transform.right * - 1;
        }
        // Else use velocity
        else
        {
            // Get XZ direction of body velocity
            legRotation = Vector3Helpers.GetXZDirection(_body.velocity);

            // Reverse leg direction vector if the difference between the angle of the torso's forward position and the attempted velocity direction is > 90 degrees
            var torsoVelocityDirectionDiff = Vector3.Angle(_torso.transform.forward, legRotation);
            if (torsoVelocityDirectionDiff > 90.0f)
            {
                legRotation *= -1;
            }
        }

        // Check if there's any leg rotation at all
        bool anyDirection = legRotation.x.Abs() != 0.0f || legRotation.z.Abs() != 0.0f;

        // Check if there's any player input
        bool anyPlayerInput = horizontalInput.Abs() + verticalInput.Abs() != 0.0f;

        // Apply rotation
        if (anyDirection && anyPlayerInput || !_moveScript.IsGrounded || _moveScript.IsSliding)
        {
            // Rotate the legs by the legRotation vector
            _legs.transform.rotation = Vector3Helpers.LookTowardsRotatedVector(_legs.transform.forward, legRotation, 5 * Time.deltaTime, 0.0f);
        }

        ClampLegs();
    }

    /// <summary>
    /// Clamp leg angles > 90 degrees from the torso
    /// </summary>
    private void ClampLegs()
    {
        float torsoLegsDirectionDiff = Vector3.Angle(_torso.transform.forward, _legs.transform.forward);
        if (torsoLegsDirectionDiff > 90.0f)
        {
            float torsoRightDiff = Vector3.Angle(_torso.transform.right, _legs.transform.forward);
            float torsoLeftDiff = Vector3.Angle(_torso.transform.right * -1, _legs.transform.forward);

            _legs.transform.rotation = torsoRightDiff < torsoLeftDiff
                ? Quaternion.LookRotation(_torso.transform.right)
                : Quaternion.LookRotation(_torso.transform.right * -1);
        }
    }
}
